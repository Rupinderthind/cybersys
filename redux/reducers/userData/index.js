import { OPEN_TOAST, SAVE_REGISTER_USER } from '../../actions/ActionTypes';
const initialstate = {
    registerDetail: {
        profile_for: '1',
        gender: '1',
        country: '2',
        state: '83',
        city: '6037',
        dob: '2019-09-15',
        education: '2',
        occupation: 'test',
        income: '10000',
        email: 'test5612@mail.com',
        password: 'test56123',
        religion: '1',
        mobile: '66666666666',
        marital_status: '1',
        mother_tongue: '2',
        live_in: 'chd',
        first_name: 'test',
        last_name: 'test',
        user_id: 'test561',
        security_question: '1',
        security_answer: 'test',
        action: 'registerUser'
    },
    toast_msg: ''
 }
export default (state = initialstate, action) => {
    switch (action.type) {
        case OPEN_TOAST:
            return { ...state, toast_msg: action.payload.toast_msg };
        case SAVE_REGISTER_USER:
            return { ...state, registerDetail: action.payload };
        default:
            return state;
    }
}



