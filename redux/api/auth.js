import MakeTheApiCall, { GenerateOptions } from "./ApiCalls";

export const contactus = data => {
  console.log(data, "data");
  var options = GenerateOptions("sendemail", "POST", data);
  return dispatch => {
    return MakeTheApiCall(options)
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return error.response.data;
      });
  };
};
