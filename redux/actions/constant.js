// export const API_URL = 'https://thinkmatters.softuvo.xyz/api';

// export const API_CHAT_URL = 'http://localhost:3003';

export const API_URL = 'https://testchoose.freesite.host/wp-content/plugins/chooseurmatchapi/chooseurmatchapi.php';

// fontSizes
export const fontSize15 = 15;
export const fontSize19 = 19;
export const fontSize12 = 12;
export const fontSize13 = 13;
export const fontSize16 = 16;
export const fontSize14 = 14;
export const fontSize10 = 10;
export const fontSize22 = 22;
export const fontSize21 = 21;
export const fontSize9 = 9;
export const fontSize18 = 18;
export const fontSize11 = 11;
export const fontSize24 = 24;
export const fontSize32 = 28;
export const fontSize8 = 8;


// colors
export const primaryColor = '#e11f3b';
export const whiteColor = '#fff';
export const blackColor = '#000';
export const greyColor = '#515250';


// font-family

export const fontFamily = 'verdana';
