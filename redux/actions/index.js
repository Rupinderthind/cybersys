import axios from 'axios'
import { API_URL } from './constant';
import { SAVE_REGISTER_USER, OPEN_TOAST } from "./ActionTypes";
import {AsyncStorage} from 'react-native'
var qs = require('qs');
export const apiCall = (data) => {
    return async dispatch => {
        return new Promise(
            (resolve, reject) => 
            axios.post(`${API_URL}`, qs.stringify(data),{
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .then(res => {
                return resolve(res)
            })
            .catch((error) => {
                return reject({...error})
            })
        )
    }
}

export const apiCalls = (data) => {
    console.log(qs.stringify(data),"dataInside")
    return async dispatch => {
        return new Promise(
            (resolve, reject) => 
            axios.post(`${API_URL}`, qs.stringify(data),{
                headers: {}
            })
            .then(res => {
                return resolve(res)
            })
            .catch((error) => {
                return reject({...error})
            })
        )
    }
}

export const openToast = data => {
    return {
        type: OPEN_TOAST,
        payload: {
            toast_msg: data
        }
    }
}

export const saveRegisterUser = data => {
    console.log(data, "data12222");
    return {
      type: SAVE_REGISTER_USER,
      payload: data
    };
  };


