import { createStackNavigator, createAppContainer } from "react-navigation";
import Home from "../Views/Home";
import StartScreen from "../Views/Auth/StartScreen";
import BarcodeScannerSetup from '../Views/Others/BarcodeScannerSetup'


const AppNavigator = createStackNavigator(
    {
      Home: Home,
      StartScreen: StartScreen,
      BarcodeScannerSetup: BarcodeScannerSetup
    },
    {
      initialRouteName: "StartScreen",
      defaultNavigationOptions: {
        gesturesEnabled: false
      },
    }
);


const Router = createAppContainer(AppNavigator);

export default Router