import React from 'react';
import { View, Text, StyleSheet, Dimensions, WebView, Alert, AsyncStorage, Image, TouchableOpacity } from 'react-native';
import { Container, Content, Footer, Icon } from 'native-base';
import MainFooter from '../../components/Common/Footer'
var { height, width } = Dimensions.get('window');

export default class BarcodeScannerSetup extends React.Component{
  static navigationOptions = {
      header: null
  }

  goBack(){
    this.props.navigation.navigate('StartScreen')
  }

  render(){
    let { navigation } = this.props;
    var url = navigation.getParam('urlvalue', 'https://www.bing.com');
    return(
        <Container style={styles.mainContainer}>
            <Content>
                <View style={styles.setupContainer}>
                	<View style={styles.barcodeContainer}>
                  	<Text style={styles.barcodeText}>Scan barcode then pair device</Text>
                  	<Image source={require('../../assets/images/barcode.png')} style={styles.barcodeImage} />
                	</View>
                	<View style={styles.barcodeContainer}>
                  	<Text style={styles.barcodeText}>Scan barcode then pair device</Text>
                  	<Image source={require('../../assets/images/barcode.png')} style={styles.barcodeImage} />
                	</View>
                </View>
            </Content>
            <Footer style={styles.backButtonCon}>
            	<TouchableOpacity style={styles.backButton} onPress={this.goBack.bind(this)}>
            		<Text style={styles.backText}>BACK</Text>
            	</TouchableOpacity>
            </Footer>
        </Container>
      )
    }
}
const styles = StyleSheet.create({
   mainContainer:{
    backgroundColor: '#fff',
    paddingTop: 30
   },
   setupContainer:{
    padding: 15
   },
   barcodeText: {
   	fontSize: 20
   },
   barcodeImage: {
   	width: 250,
   	height: 60,
   	marginTop: 10,
   	alignSelf: 'center'
   },
   barcodeContainer: {
   	marginBottom: 50
   },
   backButtonCon: {
   	backgroundColor: '#fff',
   	borderTopWidth: 0,
   	padding: 10
   },
   backButton: {
   	backgroundColor: '#fff',
   	width: '100%',
   	height: 35,
   	alignItems: 'center',
   	borderRadius: 4,
   	justifyContent: 'center'
   },
   backText: {
   	color: '#000'
   }
})