export const injectedJS = `
  function print_receipt(){
    let location = document.getElementById("receipt_wrapper"); 
    window.ReactNativeWebView.postMessage(location.innerHTML)
  }
  window.addEventListener("message",function(){
    let bodyHtml;
    if((window.location.pathname == '/index.php/sales/complete') || ((window.location.pathname).includes('/index.php/sales/receipt'))){
      bodyHtml = document.getElementById("receipt_wrapper");
    } else {
      bodyHtml = document.getElementsByTagName("BODY")[0];
    }
    window.ReactNativeWebView.postMessage(bodyHtml.innerHTML)
  })

  $('#print_button, .print_button').click(function(){
    bodyHtml = document.getElementsByTagName("BODY")[0];
    window.ReactNativeWebView.postMessage(bodyHtml.innerHTML)
  });
  true;
`