import React from 'react';
import { View, Text, StyleSheet, Dimensions, Alert, AsyncStorage, Modal } from 'react-native';
import { Container, Content, Footer, Icon } from 'native-base';
import MainFooter from '../../components/Common/Footer'
import QRCodeScanner from '../../components/Common/QRCodeScanner'
import { print } from './css';
import { WebView } from 'react-native-webview';
import RNPrint from 'react-native-print';
import {injectedJS} from './integratedJs'
var { height, width } = Dimensions.get('window');

export default class Home extends React.Component{
  static navigationOptions = {
    header: null
  }

  constructor(props){
    super(props)
    this.state = {
      currentKey: 1,
      openScanner: false
    }
  }

  onGoBack() {
    this.refs['mainWebView'].goBack();
  }

  onRefresh() {
    let {currentKey} = this.state;
    this.setState({ currentKey: currentKey + 1 });
  }

  onForward() {
    this.refs['mainWebView'].goForward();
  }

  onPrint(){
    this.refs['mainWebView'].postMessage()
  }

  openBarcodeScanner(){
    this.setState({openScanner: true})
  }

  onSetup() {
    Alert.alert(
      'Reset',
      'Are you sure you want to reset the app?',
      [
        {text: 'Cancel',style: 'cancel'},
        {text: 'OK', onPress: () => this.clearAsyncStorage() },
      ],
      {cancelable: false},
    );
  }

  clearAsyncStorage(){
    AsyncStorage.removeItem('@mystoreurl', '');
    this.props.navigation.navigate('StartScreen');
  }

  async print(e){
    let css = '<style>'+print +'</style>'
    let currentHtml = '<body>'+e.nativeEvent.data+'</body>'
    let htmlToPrint = css + currentHtml
    await RNPrint.print({
      html: htmlToPrint
    })
  }

  render(){
    let currentComponent = this;
    let { navigation } = this.props;
    var url = navigation.getParam('urlvalue', 'http://posdemo.cybersyskw.com/');
    return(
      <Container style={styles.mainContainer}>
        <Content>
          <View style={styles.webViewContainer}>
            <WebView 
              originWhitelist={['*']}
              source={{ uri: url }}
              ref={'mainWebView'} 
              key={this.state.currentKey}
              startInLoadingState={true}
              injectedJavaScript={injectedJS}
              onMessage={m => this.print(m)}
            />
          </View>
        </Content>
        <MainFooter 
          onGoBack={this.onGoBack.bind(this)}
          onRefresh={this.onRefresh.bind(this)}
          onForward={this.onForward.bind(this)}
          onPrint={this.onPrint.bind(this)}
          onOpenBarCodeScanner={this.openBarcodeScanner.bind(this)}
          onSetup={this.onSetup.bind(this)}
        />
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.openScanner}
          presentationStyle="overFullScreen"
        >
          <QRCodeScanner modalref={currentComponent} webviewref={this.refs['mainWebView']} />
        </Modal>
      </Container>
    )
  }
}
const styles = StyleSheet.create({
   mainContainer:{
    backgroundColor: '#eaebef'
   },
   webViewContainer:{
    paddingTop: 25,
    height: height - 55
   }
})