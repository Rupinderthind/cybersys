import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { Container, Content, Footer, Icon } from 'native-base';
import TextField from '../../components/Input/TextField'
var { height, width } = Dimensions.get('window');
export default class StartScreen extends React.Component{
  static navigationOptions = {
    header: null
  }
  constructor(props){
    super(props)
    this.state = {
      inputurl: ''
    }
  }

  componentWillMount(){
    this._retrieveData();
  }

  handleURL = (urltext) => {
    this.setState({ inputurl: urltext })
  }

  _storeData = async (storeurl) => {
    try {
      await AsyncStorage.setItem('@mystoreurl', storeurl);
    } catch (error) {
      // Error saving data
    }
  };

  async _retrieveData() {
    try {
      const value = await AsyncStorage.getItem('@mystoreurl');
      if (value !== null) {
        this.props.navigation.navigate('WebPage', { urlvalue: value });
      }
    } catch (error) {
      console.log('we donot have data');
    }
  };

    
  openURL = () => {
    if (this.state.inputurl == ''){
      alert('Please enter an url');
    } else {
      var url;
      url = this.state.inputurl;  
      if (!!url && !!url.trim()) {
        url = url.trim();
        if (!/^(https?:)?\/\//i.test(url)) {
          url = 'http://' + url;
        }
      } else {
          //Handle empty url
      }
      this._storeData(url);
      this.props.navigation.navigate('Home', { urlvalue: url });
    }
  }

  goToBarcodeScreen(){
    this.props.navigation.navigate('BarcodeScannerSetup')
  }

  render(){
    let { navigation } = this.props;
    return(
      <Container style={styles.container}>
        <Content>
          <View style={styles.logoContainer}>
            <Image source={ require('../../assets/images/logo.png')} style={styles.logo} />
          </View>
          <View style={styles.formContainer}>
            <TextField 
              placeholder="http://YourStoreURL" 
              onChange={this.handleURL}
            />
            <TouchableOpacity style={styles.whiteButton} onPress = {() => this.openURL()}>
              <Text style={styles.registerText}>Continue</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.whiteButton} onPress={this.goToBarcodeScreen.bind(this)}>
              <Text style={styles.registerText}>Setup Barcode Scanner</Text>
            </TouchableOpacity>
          </View>                
        </Content>
      </Container>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: '#007afe'
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 100,
    marginBottom: 30
  },
  logo: {
    width: 165,
    height: 80
  },
  formContainer:{
    width: '100%',
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 40
  },
  registerText:{
    color: '#007afe'
  },
  whiteButton:{
    backgroundColor: '#fff',
    borderRadius: 4,
    marginTop: 20,
    padding: 12,
    paddingLeft: 15,
    paddingRight:15,
    alignItems: 'center'
  }
})