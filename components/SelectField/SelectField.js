import React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import { fontFamily, greyColor, fontSize12, blackColor } from '../../redux/actions/constant';
import RNPickerSelect from 'react-native-picker-select';

export default class SelectField extends React.Component{
    onChange = (e,k,l) => {
        console.log(this.props.keyCode,"this.props.key")
        this.props.onChange(e,k,this.props.keyCode)
    }
    render(){
        let { label, onChange, value, item } = this.props;
        console.log(value,"hhhh")
        return(
            <View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={styles.simpleText}>{label}</Text>
                    <View style={styles.absStarView}><Text style={styles.starText}>*</Text></View>
                </View>
                <RNPickerSelect
                    onValueChange={(e,i) => this.onChange(e,i)}
                    items={item}
                    style={styles.textInput}
                >
                    <View style={styles.textInput}>
                        <Text>{value}</Text>
                    </View>
                
                </RNPickerSelect>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    absStarView: {
        marginLeft: 3
    },
    simpleText: {
        color: greyColor,
        fontSize: fontSize12,
        fontFamily: fontFamily
    },
    starText: {
        fontSize: 8
    },
    textInput: {
        borderBottomWidth: 0.5,
        height: 30,
        borderColor: greyColor,
        color: blackColor,
        justifyContent: 'center'
    }
})