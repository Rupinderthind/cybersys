import React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import { fontFamily, greyColor, fontSize12, blackColor } from '../../redux/actions/constant';

export default class TextField extends React.Component{
    render(){
        let { label, secureTextEntry, maxLength, onChange, value, type, onFocus, placeholder } = this.props;
        return(
            <View>
                <TextInput placeholder={placeholder} style={styles.textInput} secureTextEntry={secureTextEntry} autoCapitalize = 'none' maxLength={maxLength} onChangeText={onChange} value={value} keyboardType={type} onFocus={onFocus}/>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    textInput: {
        borderBottomWidth: 0,
        height: 40,
        color: blackColor,
        backgroundColor: '#fff',
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 4
    }
})