import React from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { fontSize15, primaryColor, whiteColor } from '../../redux/actions/constant';
var { height, width } = Dimensions.get('window');
export default class CombinedButton extends React.Component{
    render(){
        let { leftLabel, rightLabel, onLeftPress, onRightPress } = this.props
        return(
            <View style={styles.buttonConatiner}>
                <TouchableOpacity style={styles.innerButtonView} onPress={onLeftPress}>
                    <Text style={styles.signupText}>{leftLabel}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.innerButtonView,styles.innerRightButton]} onPress={onRightPress}>
                    <Text style={styles.registerText}>{rightLabel}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    buttonConatiner: {
        width: '100%',
        elevation: 0,
        shadowColor: "#000000",
        shadowOpacity: 0.4,
        shadowRadius: 5,
        shadowOffset: { height: 2, width: 1 },
        borderRadius: 5,
        flexDirection: 'row'
    },
    signupText: {
        color: primaryColor,
        fontSize: fontSize15
    },
    registerText: {
        color: whiteColor,
        fontSize: fontSize15
    },
    innerButtonView: {
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
        width: '50%',
        backgroundColor: whiteColor,
        overflow: 'hidden',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    innerRightButton: {
        borderBottomRightRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: primaryColor,
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
    }

})