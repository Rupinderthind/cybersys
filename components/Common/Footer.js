import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput } from 'react-native';
import { Container, Content, Footer, FooterTab } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor } from '../../redux/actions/constant';
var { height, width } = Dimensions.get('window');

export default class MainFooter extends React.Component{
    static navigationOptions = {
        header: null
    }
    render(){
        let { navigation } = this.props;
        return(
          <Footer styl={styles.footerContainer}>
            <FooterTab style={styles.footerTabStyle}>
              <View style={styles.footerItem}>
                <TouchableOpacity onPress={this.props.onGoBack.bind(this)}>
                  <Image source={require('../../assets/images/back.png')} style={{width: 20, height: 20}} />
                </TouchableOpacity>
              </View>
              <View style={styles.footerItem}>
                <TouchableOpacity onPress={this.props.onRefresh.bind(this)}>
                  <Image source={require('../../assets/images/refresh.png')} style={{width: 20, height: 18}} />
                </TouchableOpacity>
              </View>
              <View style={styles.footerItem}>
                <TouchableOpacity onPress={this.props.onForward.bind(this)}>
                  <Image source={require('../../assets/images/right-arrow.png')} style={{width: 18, height: 18}} />
                </TouchableOpacity>
              </View>
              <View style={styles.footerItem}>
                <TouchableOpacity onPress={this.props.onPrint.bind(this)}>
                  <Image source={require('../../assets/images/printer.png')} style={{width: 20, height: 20}} />
                </TouchableOpacity>
              </View>
              <View style={styles.footerItem}>
                <TouchableOpacity onPress={this.props.onOpenBarCodeScanner.bind(this)}>
                  <Image source={require('../../assets/images/photo-camera.png')} style={{width: 20, height: 20}} />
                </TouchableOpacity>
              </View>
              <View style={styles.footerItem}>
                <TouchableOpacity onPress={this.props.onSetup.bind(this)}>
                  <Text style={styles.menuText}>Setup</Text>
                </TouchableOpacity>
              </View>
            </FooterTab>
          </Footer>
        )
    }
}
const styles = StyleSheet.create({

    footerContainer: {
      flexDirection: 'row',
      backgroundColor: 'red'
    },
    footerItem: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    roundBtn: {
      width: 65,
      height: 65,
      borderRadius: 40,
      backgroundColor: 'red',
      alignItems: 'center',
      justifyContent: 'center',
      top: -15,

      elevation: 7,
      shadowColor: "#000000",
      shadowOpacity: 0.4,
      shadowRadius: 45,
      shadowOffset: { height: 2, width: 1 },
    },
    btnText: {
      color: '#fff',
      fontSize: 40,
      top: -3
    },
    footerTabStyle: {
      backgroundColor: '#fff'
    },
    menuText: {
      color: '#007afe',
      borderBottomWidth: 1,
      borderColor: '#007afe'
    }
})