import * as React from 'react';
import { Text, View, StyleSheet, Button, Dimensions, Image } from 'react-native';

import scanBarcode from 'react-native-scan-barcode';
const { width } = Dimensions.get('window')
const qrSize = width * 0.7


const run = `
      document.body.style.backgroundColor = 'blue';
      true;
    `;
export default class QRCodeScanner extends React.Component {
  state = {
    hasCameraPermission: null,
    scanned: false,
  };

  async componentDidMount() {
    
    this.getPermissionsAsync();
  }

  getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  };

  render() {
    const { hasCameraPermission, scanned } = this.state;

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      <View
        style={styles.container}>
        <scanBarcode
          onBarCodeRead={scanned ? undefined : this.handleBarCodeScanned}
          >
          <Image
            style={styles.qr}
            source={require('../../assets/images/QR.png')}
          />
          <View style={styles.barcodetext}>
            <Text style={styles.InfoText}>Display the barcode in the middle of the window for a successful scan.</Text>
            <Text style={styles.canceltext} onPress={ () => this.props.modalref.setModalVisible(!this.props.modalref.state.modalVisible) }>Cancel</Text>
          </View>
        </scanBarcode>
      </View>
    );
  }

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({ scanned: true });
    this.props.modalref.setModalVisible(!this.props.modalref.state.modalVisible); //close the modal
    console.log(data, 'data')
    data = data.split(' ');
    data = data[data.length - 1]
    console.log(data, 'datassssssss')
    this.props.webviewref.injectJavaScript(
      `
      if(window.location.pathname == '/index.php/sales' || window.location.pathname == '/index.php/receivings'){
        document.getElementById('item').value = ${data}
        var e2 = jQuery.Event("keypress");
        e2.which = 13;
        e2.keyCode = 13;
        $("#item").trigger(e2);
      } else {
        $('#search').val(${data});
        true;
        var e = jQuery.Event("keypress");
        e.which = 13;
        e.keyCode = 13;
        $("#search").trigger(e);
      }
      true;
      `
    )
  };
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'gray'
    },
    barcodetext: {
      marginTop: 20,
      backgroundColor: 'rgba(0, 0, 0, .6)',
      padding: 10
    },
    qr: {
      marginTop: '20%',
      marginLeft: '15%',
      width: qrSize,
      height: qrSize,
    },
    InfoText:{
      color: '#fff',
      textAlign: 'center',
      marginTop: 10
   },
   canceltext:{
    color: 'red',
    textDecorationLine: 'underline',
    fontSize: 16,
    textAlign: 'center',
    marginTop: 10
   },
   barCodeRotate: {
    transform: [{
      rotate: '-180deg'
    }],
    backgroundColor: 'red'
   }
  });
